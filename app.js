var express = require("express"),
  app = express(),
  bodyParser = require("body-parser"),
  mongoose = require("mongoose"),
  Blog = require("./models/blog"),
  User = require("./models/user"),
  passport = require("passport"),
  LocalStrategy = require("passport-local");

require("dotenv").config();

mongoose.connect(process.env.MONGO_STRING, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static(__dirname + "/public"));

// passport configuration
app.use(
  require("express-session")({
    secret: process.env.SECRET,
    resave: false,
    saveUninitialized: false,
  })
);
app.use(passport.initialize());
app.use(passport.session());
// passport-local-mongoose dan geliyor authenticate methodu.
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());
// currentUser ı her route'da tanımlamak için kolay yolu.
app.use(function (req, res, next) {
  res.locals.currentUser = req.user;
  next();
});
// trial
passport.authenticate("local", { successFlash: "Welcome!" });

app.set("view engine", "ejs");

/*
var blogs = {
	title:"Caption",
	image:"https://goinswriter.com/wp-content/uploads/desk-writing-laptop-notebook.jpg",
	text:"Ben bir blog yazısıyım"
}
*/

app.get("/blogs", function (req, res) {
  //burdai Blog modelden geliyor
  Blog.find({}, function (err, allBlogs) {
    if (err) {
      console.log(err);
    } else {
      //console.log(allBlogs)
      res.render("home", { Blogs: allBlogs });
      // home a göndercem allblogsları Blog adı altında kullanıcam
    }
  });
});

app.get("/new", function (req, res) {
  res.render("blogs/new");
});
// --- BLOG POST ROUTE ---

app.post("/blogs", function (req, res) {
  var blogImage = req.body.formBlogImage,
    blogText = req.body.formBlogText,
    blogTitle = req.body.formBlogTitle;

  var newBlog = { title: blogTitle, image: blogImage, text: blogText };

  Blog.create(newBlog, function (err, newlyCreated) {
    if (err) {
      console.log(err);
    } else {
      //console.log(newlyCreated)
      res.redirect("/blogs");
    }
  });
});

// --- BLOG SHOW ROUTE ---

app.get("/blogs/:id", function (req, res) {
  Blog.findById(req.params.id, function (err, foundBlog) {
    if (err) {
      console.log(err);
    } else {
      res.render("show", { Blog: foundBlog });
    }
  });
});

// ==================
//    AUTH ROUTES
// ==================

app.get("/register", function (req, res) {
  res.render("register");
});

app.post("/register", function (req, res) {
  var newUser = new User({ username: req.body.username });
  User.register(newUser, req.body.password, function (err, user) {
    if (err) {
      console.log(err);
      return res.render("register");
    }
    passport.authenticate("local")(req, res, function () {
      res.redirect("/blogs");
    });
  });
});

// show login form!

app.get("/login", function (req, res) {
  res.render("login");
});

//	handling login logic
// login için middle ware function ımız gerekiyordu...
// app.post("/login", middleware, callback)
// authenticate methodunu passport.use(new LocalStrategy(User.authenticate())); da tanımladık

app.post(
  "/login",
  passport.authenticate("local", {
    successRedirect: "/blogs",
    failureRedirect: "/login",
  }),
  function (req, res) {}
);

// show logout

app.get("/logout", function (req, res) {
  req.logout();
  res.redirect("/blogs");
});

app.listen("8080", function () {
  console.log("Working");
});
